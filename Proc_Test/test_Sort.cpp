#include "stdafx.h"
#include "CppUnitTest.h"
#include "../PreLab1/shape_atd.h"
#include "../PreLab1/container_atd.h"
#include "../PreLab1/circle_atd.h"
#include "../PreLab1/rectangle_atd.h"
#include "../PreLab1/triangle_atd.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
namespace simple_shapes {

	void Init(container&c);
	void add_Shape(container &c, shape *sp);
	void Sort(container &c);
	void color_In(shape &s, int colorNumber);
	void Out(container &c, ofstream &ofst);
	
}
using namespace simple_shapes;

namespace SortTest
{
	TEST_CLASS(SortTest)
	{
	public:
		TEST_METHOD(TestSort)
		{
			container c;
			Init(c);

			shape *tr = new shape;
			tr->k = shape::key::TRIANGLE;
			tr->q.x1 = 0;
			tr->q.x2 = 0;
			tr->q.y1 = 0;
			tr->q.y2 = 3;
			tr->q.z1 = 4;
			tr->q.z2 = 0;
			color_In(*tr, 3);
			tr->density = 6.0;

			shape *r1 = new shape;
			r1->k = shape::key::RECTANGLE;
			r1->r.x1 = 0;
			r1->r.x2 = 2;
			r1->r.y1 = 0;
			r1->r.y2 = 2;
			color_In(*r1, 1);
			r1->density = 1.3;

			shape *t1 = new shape;
			t1->k = shape::key::CIRCLE;
			t1->t.x = 0;
			t1->t.y = 0;
			t1->t.r = 1;
			color_In(*t1, 2);
			t1->density = 5.1;


			add_Shape(c, tr);
			add_Shape(c, r1);
			add_Shape(c, t1);

			container SortC;
			Init(SortC);
			add_Shape(SortC, t1);
			add_Shape(SortC, r1);
			add_Shape(SortC, tr);

			Sort(c);

			ofstream ofstc("ContTest.txt");
			ofstream ofstSort("SortTest.txt");
			Out(c, ofstc);
			Out(SortC, ofstSort);

			ifstream ifstc("ContTest.txt");
			ifstream ifstSort("SortTest.txt");
			string s;
			string exp;
			while (!ifstSort.eof())
			{
				getline(ifstSort, exp);
				getline(ifstc, s);
				Assert::AreEqual(exp, s);
			}

		}
	};
}