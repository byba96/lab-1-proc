#include "stdafx.h"
#include "CppUnitTest.h"
#include "../PreLab1/shape_atd.h"
#include "../PreLab1/container_atd.h"
#include "../PreLab1/circle_atd.h"
#include "../PreLab1/rectangle_atd.h"
#include "../PreLab1/triangle_atd.h"
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace simple_shapes {

		float Perimeter(shape &s);
	
}
using namespace simple_shapes;

namespace PerimeterTest
 {
	TEST_CLASS(PerimeterTest)
		 {
		public:
			
				TEST_METHOD(PerimeterCircle)
				{
				float expected = 6.28;
				shape sp;
				sp.k = shape::key::CIRCLE;
				sp.t.x = 1;
				sp.t.y = 1;
				sp.t.r = 1;
				float p = Perimeter(sp);
				Assert::AreEqual(expected, p);
				}
			
				TEST_METHOD(PerimeterRectangle)
				{
				float expected = 8.0;
				shape sp;
				sp.k = shape::key::RECTANGLE;
				sp.r.x1 = 0;
				sp.r.x2 = 2;
				sp.r.y1 = 0;
				sp.r.y2 = 2;
				float p = Perimeter(sp);
				Assert::AreEqual(expected, p);
				}
			
				TEST_METHOD(PerimeterTriangle)
				{
				float expected = 12.0;
				shape sp;
				sp.k = shape::key::TRIANGLE;
				sp.q.x1 = 0;
				sp.q.x2 = 0;
				sp.q.y1 = 0;
				sp.q.y2 = 3;
				sp.q.z1 = 4;
				sp.q.z2 = 0;
				float p = Perimeter(sp);
				Assert::AreEqual(expected, p);
				}
			};
	}