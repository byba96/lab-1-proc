#include "stdafx.h"
#include <fstream>
#include "shape_atd.h"

using namespace std;

namespace simple_shapes 
{
	// ��������� ��������� ������� �������.
	float Perimeter(rectangle &r);
	float Perimeter(circle &t);
	float Perimeter(triangle &q);
	
	// ���������� ��������� ������
	float Perimeter(shape &s)
	{
		switch (s.k) {
		case shape::key::RECTANGLE:
			return Perimeter(s.r);
		case shape::key::CIRCLE:
				return Perimeter(s.t);
		case shape::key::TRIANGLE:
			return Perimeter(s.q);
		default:
			return -1;
		}
	}
} // end simple_shapes namespace