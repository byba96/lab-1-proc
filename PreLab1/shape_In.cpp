#include "stdafx.h"
#include <fstream>
#include "shape_atd.h"


using namespace std;

namespace simple_shapes
{
	// ��������� ��������� ������� �������
	void In(rectangle &r, ifstream &ist);
	void In(circle  &t, ifstream &ist);
	// ��������� ������� ��� ������ ������ �����
	void In(triangle &q, ifstream &ist);
	void color_In(shape &s, int colorNumber);
	// ���� ���������� ���������� ������ �� �����
	shape* In(ifstream &ifst)
	{
		shape *sp;
		int k;
		int c;
		ifst >> k;
		switch (k) {
		case 1:
			sp = new shape;
			sp->k = shape::key::RECTANGLE;
			In(sp->r, ifst);
			ifst >> c;
			color_In(*sp, c);
			ifst >> sp->density;
			return sp;
		case 2:
			sp = new shape;
			sp->k = shape::key::CIRCLE;
			In(sp->t, ifst);
			ifst >> c;
			color_In(*sp, c);
			ifst >> sp->density;
			return sp;
		case 3:
			sp = new shape;
			sp->k = shape::key::TRIANGLE;
			In(sp->q, ifst);
			ifst >> c;
			color_In(*sp, c);
			ifst >> sp->density;
			return sp;
		default:
			return 0;
		}
	}
} // end simple_shapes namespace