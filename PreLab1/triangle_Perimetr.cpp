#include "stdafx.h"
#include <fstream>
#include "triangle_atd.h"


using namespace std;

namespace simple_shapes 
{
	// ���������� ��������� ��������������
	float Perimeter(triangle &q) 
	{
			float a;
			float b;
			float c;
			if ((q.x1 == q.y1 && q.y1 == q.z1) || (q.x2 == q.y2 && q.y2 == q.z2) || (q.x1 == q.y1 && q.x2 == q.y2) || (q.x1 == q.y1 && q.x2 == q.y2) ||
				(q.x1 == q.z1 && q.x2 == q.z2) || (q.y1 == q.z1 && q.y2 == q.z2))
				return 0;
			else
			{
				a = sqrt((q.y1 - q.x1)*(q.y1 - q.x1) + (q.y2 - q.x2)*(q.y2 - q.x2));
				b = sqrt((q.z1 - q.x1)*(q.z1 - q.x1) + (q.z2 - q.x2)*(q.z2 - q.x2));
				c = sqrt((q.z1 - q.y1)*(q.z1 - q.y1) + (q.z2 - q.y2)*(q.z2 - q.y2));
				return a + b + c;
			}
	}
} // end simple_shapes namespace