#include "stdafx.h"
#include <fstream>
#include "rectangle_atd.h"

using namespace std;

namespace simple_shapes 
{
	// ����� ���������� �������������� � �����
	void Out(rectangle &r, ofstream &ofst) 
	{
		if (r.x1 != r.y1 && r.x2 != r.y2)
		{
			ofst << "It is Rectangle: x1 = " << r.x1
				<< ", x2 = " << r.x2
				<< ", y1 = " << r.y1
				<< ", y2 = " << r.y2
				<< ", color: ";
		}
		else
			ofst << "Check the input data. The coordinates must be different." << endl;
	}
} // end simple_shapes namespace