#include "stdafx.h"
#include <fstream>
#include "triangle_atd.h"


using namespace std;

namespace simple_shapes 
{
	// ����� ���������� ������������ � �����
	void Out(triangle &q, ofstream &ofst)
	{
		if (q.y1 != q.x1 && q.y2 != q.x2 && q.z1 != q.x1 && q.z2 != q.x2 && q.z1 != q.y1 && q.z2 != q.y2)
		{
			ofst << "It is Triangle: x1 = "
				<< q.x1 << ", x2 = " << q.x2
				<< ", y1 = " << q.y1
				<< ", y2 = " << q.y2
				<< ", z1 = " << q.z1
				<< ", z2 = " << q.z2
				<< ", color: ";
		}
		else
			ofst << "Check the input data. The coordinates of triangle different." << endl;
	}
} // end simple_shapes namespace