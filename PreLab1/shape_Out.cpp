#include "stdafx.h"
#include <fstream>
#include "shape_atd.h"

using namespace std;

namespace simple_shapes 
{
	// ��������� ��������� ������� �������.
	void Out(rectangle &r, ofstream &ofst);
	void Out(circle  &t, ofstream &ofst);
	void Out(triangle &q, ofstream &ofst);
	void color_Out(shape &s, ofstream &ofst);

	// ����� ���������� ������� ������ � �����
	void Out(shape &s, ofstream &ofst) {
		switch (s.k) {
		case shape::key::RECTANGLE:
			Out(s.r, ofst);
			color_Out(s, ofst);
			break;
		case shape::key::CIRCLE:
			Out(s.t, ofst);
			color_Out(s, ofst);
			break;
		case shape::key::TRIANGLE:
			Out(s.q, ofst);
			color_Out(s, ofst);
			break;
		default:
			ofst << "Incorrect figure!"
				<< endl;
		}
		if (s.density > 0)
		{
		ofst << "density = " << s.density << endl;
		}
		else
			ofst << "Check the input data. Density must be greater than zero." << endl;
	}
} // end simple_shapes namespace