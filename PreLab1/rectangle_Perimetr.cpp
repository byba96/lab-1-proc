#include "stdafx.h"
#include <fstream>
#include "rectangle_atd.h"

using namespace std;

namespace simple_shapes 
{
	// ���������� ��������� ������������
	float Perimeter(rectangle &r)
	{
		if (r.x1 != r.y1 && r.x2 != r.y2)
		{
			return (abs(r.y2 - r.x2) + abs(r.y1 - r.x1)) * 2;
		}
		else
			return 0;
	}
} // end simple_shapes namespace