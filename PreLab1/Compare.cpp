#include "stdafx.h"
#include <fstream>
#include "shape_atd.h"

using namespace std;

namespace simple_shapes 
{
	// ��������� ��������� �������
	float Perimeter(shape &s);
	// C�������� ������ ���� ����������� ��������
	bool Compare(shape *first, shape *second) 
	{
		return Perimeter(*first) < Perimeter(*second);
	}
} // end simple_shapes namespace