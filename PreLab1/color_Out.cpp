#include "stdafx.h"
#include <fstream>
#include "shape_atd.h"

using namespace std;

namespace simple_shapes 
{
	void color_Out(shape &s, ofstream &ofst)
	{
		switch (s.c)
		{
		case shape::color::RED:
			ofst << "RED" << endl;
			break;
		case shape::color::ORANGE:
			ofst << "ORANGE" << endl;
			break;
		case shape::color::YELLOW:
			ofst << "YELLOW" << endl;
			break;
		case shape::color::GREEN:
			ofst << "GREEN" << endl;
			break;
		case shape::color::BLUE:
			ofst << "BLUE" << endl;
			break;
		case shape::color::PURPLE:
			ofst << "PURPLE" << endl;
			break;
		default:
			ofst << "Check the input data. Color is not defined." << endl;
			break;
		}
	}
}