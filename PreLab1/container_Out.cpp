#include "stdafx.h"
#include <fstream>
#include "container_atd.h"

using namespace std;

namespace simple_shapes 
{
	void Out(shape &s, ofstream &ofst);
	float Perimeter(shape &s);	void Sort(container &c);
	// ����� ����������� ���������� � ��������� �����
	void Out(container &c, ofstream &ofst) 
	{
		Sort(c);
		ofst << "Container contains " << c.len
			<< " elements." << endl;
		for (int i = 0; i < c.len; i++) 
		{
			ofst << i << ": ";
			Out(*(c.cont[i]), ofst);
			if (Perimeter(*(c.cont[i]))>0)
			{
				ofst << "perimeter = " << Perimeter(*(c.cont[i])) << endl;
			}
			else
				ofst << "Check the input data. Perimeter must be greater than zero." << endl;
		}
	}
} // end simple_shapes namespace