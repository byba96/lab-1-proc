#include "stdafx.h"
#include <fstream>
#include "shape_atd.h"

using namespace std;

namespace simple_shapes 
{
	void color_In(shape &s, int colorNumber)
	{
		if (colorNumber == 1)
		{
			s.c = shape::color::RED;
		}
		if (colorNumber == 2)
		{
			s.c = shape::color::ORANGE;
		}		
		if (colorNumber == 3)
		{
			s.c = shape::color::YELLOW;
		}
		if (colorNumber == 4)
		{
			s.c = shape::color::GREEN;
		}
		if (colorNumber == 5)
		{
			s.c = shape::color::BLUE;
		}
		if (colorNumber == 6)
		{
			s.c = shape::color::PURPLE;
		}	
	}
}