#include "stdafx.h"
#include <fstream>
#include "circle_atd.h"

using namespace std;

namespace simple_shapes
{
	// ����� ���������� ������������ � �����
	void Out(circle &t, ofstream &ofst)
	{
		if (t.r > 0)
		{
			ofst << "It is Circle: x = "
				<< t.x << ", y = " << t.y
				<< ", r = " << t.r << ", color: ";
		}
		else
			ofst << "Check the input data. The radius must be greater than zero." << endl;
	}
} // end simple_shapes namespace