#include "stdafx.h"
#include <fstream>
#include "circle_atd.h"

using namespace std;

namespace simple_shapes 
{
	// ���������� ��������� ��������������
	float Perimeter(circle &t)
	{
		if (t.r > 0)
		{
			return t.r*6.28;
		}
		else
			return 0;
	}
} // end simple_shapes namespace