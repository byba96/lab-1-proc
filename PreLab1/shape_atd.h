#ifndef __shape_atd__
#define __shape_atd__
// ����������� ����������� ����� ������
#include"rectangle_atd.h"
#include "circle_atd.h"
#include "triangle_atd.h"

namespace simple_shapes 
{
	// ���������, ���������� ��� ��������� ������
	struct shape {
		enum color { RED, ORANGE, YELLOW, GREEN, BLUE, PURPLE };
		color c;
		// �������� ������ ��� ������ �� �����
		enum key { RECTANGLE, CIRCLE, TRIANGLE };
		key k; // ����
		float density;
		// ������������ ������������
		union { // ���������� ���������
			rectangle r;
			circle t;
			triangle q;
		};
	};
} // end simple_shapes namespace
#endif